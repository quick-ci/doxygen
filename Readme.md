# Doxygen GitLab CI

Let GitLab CI generate [doxygen] documentations and publish to GitLab Pages.

## Features

- create [doxygen] documentation within CI pipelines
- modern ui styles using [doxygen-awesome]
- serve documentation with gitlab pages
- automated versioning of documentation

## Usage

For a quick-start just include `doxygen-ci.yml` in the `.gitlab-ci.yml` like this:

```yaml
include:
  - project: quick-ci/doxygen
    ref: main
    file: doxygen-ci.yml
```

After you pushed the changes simply create a new tag and wait for the pipeline to finish. The versioned documentation page will now be served with the gitlab page (`https://<namespace>.gitlab.io/<project-name>/<tag>` and `https://<namespace>.gitlab.io/<project-name>/latest` for the latest tag). 

## Configuration

The documentation can be customized using the following variables in the gitlab ci script:

| variable | description | default |
|---|---|---|
| `DOXYGEN_PROJECT_NAME` | name of the project | `$CI_PROJECT_TITLE` |
| `DOXYGEN_PROJECT_DESCRIPTION` | a short description of the project | `$CI_PROJECT_DESCRIPTION` |
| `DOXYGEN_PROJECT_TAG` | current project version/tag | `$CI_COMMIT_REF_SLUG` |
| `DOXYGEN_PROJECT_LOGO` | link to project logo | `-` |
| `DOXYGEN_MAINPAGE` | entry page of documentation | `Readme.md` |
| `DOXYGEN_EXAMPLE_PATH` | path to source code examples | `examples/` |
| `DOXYGEN_SOURCE_PATH` | (list of) path containing the source codes | `include/ source/` |
| `DOXYGEN_DOCS_PATH` | (list of) path to documentation files | `docs/ documentation/` |
| `DOXYGEN_INPUT` | input files for doxygen to process | `$(DOXYGEN_MAINPAGE) $(DOXYGEN_SOURCE_PATH) $(DOXYGEN_DOCS_PATH)` |


[doxygen]: https://www.doxygen.nl/
[doxygen-awesome]: https://jothepro.github.io/doxygen-awesome-css/