#!/bin/python3
import json
import os

pubDir = os.path.abspath("./public")
versionFile = pubDir + "/versions.json"
redirectsFile = pubDir + "/_redirects"
commitTag = os.getenv('CI_COMMIT_TAG')
pagesUrl = os.getenv('CI_PAGES_URL')
currentTagPagesUrl = pagesUrl + "/" + commitTag
latestPagesUrl = pagesUrl + "/latest"

if not os.path.isfile(versionFile):
    with open(versionFile, "w") as f:
        f.write("{}")

versions = {}
with open(versionFile, "r") as f:
    versions = json.load(f)

versions[commitTag] = currentTagPagesUrl
with open(versionFile, "w") as f:
    json.dump(versions, f)

with open(redirectsFile, "w") as f:
    f.write("/" + str(os.getenv('CI_PROJECT_NAME')) + "/latest/* /" + str(os.getenv('CI_PROJECT_NAME')) + "/" + str(os.getenv('CI_COMMIT_TAG')) + "/:splat 200")